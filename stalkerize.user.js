// ==UserScript==
// @name         Stalkerize
// @namespace    http://tampermonkey.net/
// @version      0.2.1
// @description  Stalkerize, a melhor ferramenta!
// @author       Ratodisco
// @include      http://atelier801.com/topic?f=*
// @include      http://atelier801.com//topic?f=*
// @grant        none
// ==/UserScript==

$(document).ready(function() {
    
    $('body').append($('<div/>', {
        id: 'modal-stalker',
        class: 'modal fade',
        role: 'dialog',
        style: 'z-index: -10;'
    }));

    $('#modal-stalker').append(
        '<div class="modal-dialog">'+
            '<div class="modal-content">'+
                '<div class="modal-header">'+
                    '<button type="button" class="close" data-dismiss="modal">&times;</button>'+
                    '<h4 class="modal-title">Stalkerize!</h4>'+
                '</div>'+
                '<div class="modal-body">'+
                    '<label for="nome-candidato" style="font-weight:bold">Nick do stalkeado:</label>'+
                    '<input type="text" class="form-control" id="nick" placeholder="Insira o nick do stalkeado">'+
                    '<label for="colorpicker" style="font-weight:bold">Cor: <span id="test-color" style="font-weight:bold;color:#E68D43;">Teste de cor</span></label>'+
                    '<select class="form-control" id="colorpicker">'+
                    '<option value="#009D9D">#009D9D</option>'+
                    '<option value="#606090">#606090</option>'+
                    '<option value="#98E2EB">#98E2EB</option>'+
                    '<option value="#CB546B">#CB546B</option>'+
                    '<option value="#EDCC8D">#EDCC8D</option>'+
                    '<option value="#FEB1FC">#FEB1FC</option>'+
                    '<option value="#2E72CB">#2E72CB</option>'+
                    '<option value="#6C77C1">#6C77C1</option>'+
                    '<option value="#BABD2F">#BABD2F</option>'+
                    '<option value="#EB1D51">#EB1D51</option>'+
                    '<option value="#E68D43" selected>#E68D43</option>'+
                    '<option value="#30BA76">#30BA76</option>'+
                    '<option value="#92CF91">#92CF91</option>'+
                    '<option value="#C2C2DA">#C2C2DA</option>'+
                    '<option value="#ED67EA">#ED67EA</option>'+
                    '<option value="#F0A78E">#F0A78E</option>'+
                    '</select>'+
                '</div>'+
                '<div class="modal-footer">'+
                    '<button type="button" class="btn btn-success" id="stalkerize">Stalkerize</button>'+
                    '<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>'+
                '</div>'+
            '</div>'+
        '</div>');

    var element = document.getElementById("outils_message_reponse");
    var BBCodeHTML = element.innerHTML;
    var buttonContainer = document.createElement("div");
    var button = document.createElement('button');

    element.innerHTML = BBCodeHTML;
    buttonContainer.className = "btn-group groupe-boutons-barre-outils";
    button.className = "btn btn-reduit";
    button.type = "button";
    button.setAttribute("id", "zindexChange");
    button.setAttribute("data-toggle", "modal");
    button.setAttribute("data-target", "#modal-stalker");
    button.innerHTML = "Stalkerize!";

    buttonContainer.appendChild(button);
    element.appendChild(buttonContainer);
    
    $("#zindexChange").click(function() {
        $('#modal-stalker').css('z-index','');
    });
    
    $('#stalkerize').click(function() {
        stalkerize($('#nick').val());
    });

    $('#colorpicker').change(function() {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        $("#test-color").css('color', valueSelected);
    });
});

function stalkerize(stalkeado) {
    var message = $('#message_reponse');
    var lines = $('#message_reponse').val().split('\n');

    for(var i = 0;i < lines.length;i++) {
        if (lines[i].indexOf("[" + stalkeado + "]") != -1) {
            lines[i] = '[color='+ $("#colorpicker").val() +'][b]' + lines[i] + '[/b][/color]';
        }
    }
    message.val("");
    for (var x = 0; x < lines.length; x++) {
        message.val(message.val() + lines[x] + '\n');
    }
    $('#modal-stalker').modal('hide');
}

